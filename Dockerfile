FROM jupyter/datascience-notebook
RUN pip install tensorflow

USER root
RUN apt-get update -y
USER $NB_UID
