report: "report/workflow.rst"

rule all:
    input:
        ["fig1.svg"]

#rule all:
#    input:
#        "models_acc/model1.pkl",
#        "models_acc/model2.pkl"


#rule data_split:
#    input:
#        "data/raw/sample_dataset.tsv"
#    output:
#        "data/split/train_sample_dataset.tsv",
#        "data/split/test_sample_dataset.tsv"
#    shell:
#        "python scripts/train_split.py {input} {output}"

#rule train_model:
#    input:
#        "data/split/train_sample_dataset.tsv"
#    output:
#        "models/model{model_id}.pkl"
#    shell:
#        "python experiments/model{wildcards.model_id}.py {input} {output}"

#rule test_model:
#    input:
#        "data/split/train_sample_dataset.tsv",
#        "models/model{model_id}.pkl"
#    output:
#        "models_acc/model{model_id}.pkl"
#    shell:
#        "python scripts/test_model.py {input} {output}"


rule generate_figure:
    output:
        report("fig1.svg", caption="report/fig1.rst", category="Step 1")
    container:
        "docker://czentye/continuumio/anaconda3"
    shell:
        "python scripts/create_fig.py; ; cp fig1.svg {output}"

