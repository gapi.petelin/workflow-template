import pickle
import sys

import pandas as pd
from sklearn.linear_model import LinearRegression

if __name__ == "__main__":
    data_path = str(sys.argv[1])
    model_path = str(sys.argv[2])
    data = pd.read_csv(data_path, sep='\t')
    np_data = data.to_numpy()
    X = np_data[:, 1:4]
    y = np_data[:, 4]

    model = LinearRegression()
    model.fit(X, y)
    pickle.dump(model, open(model_path, "wb"))
