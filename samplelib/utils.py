import pandas as pd
import numpy as np


def split_data(raw_file, train_file, test_file):
    print('Spliting data', raw_file, 'into', train_file, 'and', test_file)
    data = pd.read_csv(raw_file, sep='\t')
    msk = np.random.rand(len(data)) < 0.8
    train = data[msk]
    test = data[~msk]
    train.to_csv(train_file, sep="\t")
    test.to_csv(test_file, sep="\t")
