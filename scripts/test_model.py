import pickle
import sys
import pandas as pd
from sklearn.metrics import mean_squared_error

if __name__ == "__main__":
    data_path = str(sys.argv[1])
    model_path = str(sys.argv[2])
    error_path = str(sys.argv[3])

    data = pd.read_csv(data_path, sep='\t')
    np_data = data.to_numpy()
    X = np_data[:, 1:4]
    y = np_data[:, 4]

    model = pickle.load(open(model_path, "rb"))
    y_ = model.predict(X)
    error = mean_squared_error(y, y_)
    pickle.dump(error, open(error_path, "wb"))
