import sys
import pandas as pd
from samplelib.utils import split_data

if __name__ == "__main__":
    raw = str(sys.argv[1])
    train = str(sys.argv[2])
    test = str(sys.argv[3])
    data = pd.read_csv(raw, sep='\t')
    split_data(raw, train, test)
