import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

packages = [
    'numpy',
    'matplotlib',
    'pandas',
    'scikit-learn',
    ]

setuptools.setup(
    name="samplelib",
    version="0.0.1",
    description="Some descripüption",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=packages,
)
